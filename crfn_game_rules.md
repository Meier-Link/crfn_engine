CRFN - Game rules
=================

This file will defines rules of the CRFN game which should be developed based
on the game engine.

Main Rules
----------

### Target

Half card / turn by turn and strategy game.

The game will turn around a board with hexagonal cells where creatures will
evolves.

Two Libraries:
* Reinforcement, which can be played from the first turn,
* The main one which will be used later and directly in game.

### Mana rules

Mana and anti-Mana are structured in 'flavours'.

Anti-mana behaviors wants to be developed.

Players will start with a fixed amount of mana. This value is the same for every
one, but some cards may improve the amount of mana available. This amount is
reinitialized at the beginning of his or her turn. 

#### Mana

5 minor flavours:
* Water: manipulating the environment, illusions
* Earth: great toughness and protection skills
* Wind: dodging and dexterity
* Fire: power, strongness and direct damages
* Mind: strategy, mind control

2 majors:
* Order: ability to create from nothing, healing
* Chaos: annihilation and sacrifices for great power

#### Anti Mana

Todo (wanted for later developments).

2 flavours:
* Vacuum
* Antimatter

### Card types

All cards has a launching cost in players' mana (it can be 0).

An action which requires the card to be tapped can be used only during player's
turn. All cards of a player are untapped at the beginning of his or her next
turn.

#### Creatures

Defined by 4 stats:
* Power: damage deals in fights
* Strength: max absorbed damages
* Energy: wanted by the creature to use his or her abilities (returned to
initial value at the beginning of each player turn)
* Mindset: Increased whenever an action succeed, decreased whenever an action
fails. It may improves creature abilities. The kind of improvement are community
dependent.

When a creature attacks, it must be tapped. It can be blocked by one or many
opponent's creature(s). When his or her attack succeed, the creature gains 1pt
of mindset.

When a creature uses an action, this one may have an enegy cost and/or require
the creature to be tapped. In case of success, the creature gains 1 + energy cost
points of mindset. In case of fail, his or her loose an amount of mindset points
equal to the energy cost of the action (0 if the energy cost was null).

#### Artifacts

They're tools which may provides some mana abilities. They're sometimes natural,
sometimes artificials.

Artifacts may have effects which:
* can be used whenever in the game,
* last during the game (until the card is removed from the game),
* can be used only when the card is tapped,
* ...
