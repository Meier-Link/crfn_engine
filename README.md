CRFN - The Role Playing Game Engine
===================================

This game engine was fistrly inspired by the need to build a specific one for
my universe.

Then, the idea rise up to create a more global game engine which can ben used
for any other game of the same kind.

Right now, I am just looking for the global structure of the database and the
code.  So for now, this file will only contain ideas of implementation.

Building/install instruction and documentation will come in when the project
will become mature.

Specs could be found in [specs file](specs.md).

Main lines
----------

Here we are building an engine for turn by turn games.

The player will be able to create a team. A team is built with characters
managed by a leader.

On each turns, the player decide which characters do actions, and which actions
this character do.

A battle is just a set of turns in a specific context.

That's why, a turn is defined by a context, where characters can do actions,
which may have effects on the environment, other characters, or the character
himself.

The game engine must enable an infinite kind of objects.

Characters
----------

A character have statistics which defines the status of the character at a given
moment.

Basic stats could be life, mana, money, power,... which may be increased,
decreased, ... along the game.  Stats are just values used to know if the
character can do or not a given action.

A character has also abilities which the player could improve when specific
events arise, such as the moment where a character gains a level.

It should be better to split characters data between stats and abilities .

Abilities
---------

### Definition

Each character of a team will have attributes, knowledges and skills, all of
them could be called 'abilities'.  All abilities may have the same kind of
attributes, such as duration (infinite for attributes and knowledges), action,
description, name, ...

Note that an ability may require a minimum level in one or more other ability to
be used by a character.

As this game engine is aimed to be used outside of CRFN, it must be possible to
define new kinds of abilities.

### Role Play Action Language

To make abilities' actions as customisable as possible, an action language
should be implemented. Then, events to launch these actions should be also
customisable. We may provide default ones such as "on game start", "on ability
change",... But events should be used every where (for example, "on
\<kind\_of\_ability\> launched/intercepted/finished/whatever you imagine/...").

Then, action language should be event driven language.  Basic actions should be
the modification of environment values (target pv/power/..., the weather, ...).
Advanced actions should be able to call to other abilities. This way, it could
be possible to define a permanent ability which do the same as an instant
ability, but on each turn, or makes possible to build complex abilities easily.

An action should be able to interact with the ability which holds the action.
For instance, the ability duration could change following events raised in the
games.

Conditional statements are also required: if/elif/else could be enough.
