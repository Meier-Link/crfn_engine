Specifications
==============

This define engin behaviors based on the readme content.

Database Model
--------------

### turn\_step

Name and order of steps of a single turn.

turn names will be used by actions to attach an action to a specific step of
the turn (basically `do <action_name> <some stuff>`)

Only id and name could be useful.

### abilities 

This table may hold main caracteristics (passive abilities), passive and active
skills. The main columns will be:
* name
* description
* duration (in number of turn. 0 for never finish, 1 for immediate skills, any
other number to make a skill which last more than one turn).
* action (This one will hold instructions aimed to be interpreted by a specific
interpreter which will be developed later).

### families

The family of any attributes:
* name
* description

### abilities\_families

n-n link between abilities and families.

### jobs

A table to hold any jobs a character may do.
The minimum consist in 'name' and 'description'.

We may add access condition (TODO: how?)

### abilities\_jobs

Some abilities may be limited to some jobs.
* attr\_id
* job\_id
* level (to indicate a minimum level in the job to get the access to the
attribute

A family\_jobs relationship can be added to add inheritence directly with kind
of abilities.

### types

A table to hold all elements (inanimate and animate) of the game.
Minimum fields are:
* name
* description

### ability\_target

Some abilities may only target some kind of types (by default, it may target
anything).

### abilities\_dependencies

Some attributes may want a minimum level of knowledge in one or many other(s)
abilities:
* attribute\_id
* dependency\_id
* level

### scenarii

Hold the metas of adventures:
* name
* description
* minimum\_level
* minimum\_team\_size
* first\_chapter\_id

### chapters

All the chapters for a scenario:
* name
* kind (something like 'battle', 'history', etc.)
* text
* event (to launch some event like a battle)

A n-n table 'next\_chapters' can be used to link a chapter to the next ones.
(Multiple because we can follow various ways). 

### characters

To hold all characters of the game:
* name
* description
* is\_leader (usefull for team of charcters under the control of the player)
* cost (0 for leaders. For others, it may be usefull to refine the team
abilities)

A 1-n table 'team' will be available to link a leader to its team.

A 'character\_type' will be usefull to hold the type(s) of a character (elf,
human, dwarf, ...).

Object model
------------

### Scenario

To simplify, any adventure is composed with scenario which is a set of
chapters.

The end of each chapter result in a choice (explicit or implicit) where the
player will have to choose the next chapter.
Implicit may occure (for instance) when a battle takes place in the chapter.
The next chapter may depend on the result of the battle. (For example, defeat
leads the player to a jail, victory to the treasure).

So, there is two main classes:
* Scenario which reflect the table in database
* Chapter, which do the same.

### Actions

There are 4 main classes:
* Character which will be used to represent actors of the action
* ActionSet which holds actions that a character will have to do
* ActionMaker which implement the "Role Player Action" language
* ActionContext which hold all environmental info like the turn number, the
action owner, the targets, ... And any information which can affect the action.

#### Character

Only one public method 'do' which takes an action in parameter.
Private methods will do checks to know if the character can do the action,
and so other actions to define.

The main attributes of this class will be the "character" from database.
They will be private (access only with a getter).

Some attributes may represent calculated attributes of the character from its
database attributes. They will be public.
For example, life points are calculated from stamina, power from strength, etc.
These values are stats which may changes along the game.

#### ActionSet

Basically, its an array of actions with some specific methods (to define).
The actions will be unstacked to the "do" method of character until its action
points are spent.

#### ActionMaker

This is basically an interpreter which translate ability actions in python.
Because we don't have hard thing to do, the main structure can be fixed to a
basic form like "verb" "subject" "operation".

Verbs can be:
* do: to execute an action
* define: to define local values (i.e. in the scope of the action)
* ...

Subject will be a var of the environment (target.stamina, player.strength,
...) or the name.

Operations will be the action to do or mathematical operators.
For instance, we can have:

    do target.stamina - player.strength.

Then, operations can be abilities called from database.
In this case, the action environment, action owner and targets must be
provided.

To improve flexibility, conditional operators can be implemented as a simple
"if", "elif", "else", "fi" suite.

Finally, operations will be stacked in a "on turn\_step ... end step" block.

Each time an action will be executed, its counter will be decremented (excepted
if it already reached 0).

The full implementation rules will be developed later.

#### ActionContext

This class has to hold as many information as possible, which can be useful for
the action, from the environment.
In the absolute, more information in the context means more elements which can
be used to build custom action, and so custom skills for characters.

This class instances will also hold attributes of the matching ability.

Action Language
---------------

Here will be the full definition of the action language.

This language will works as an event-driver language. So pieces of code will
turn around events fired from outside the script.

As explain earlier, kinds of events will come from the database to make possible
the definition of custom events.

### Main structure

A script should be structured as follow:

    """Permanent curse
        Target will get one pv each turn as long as its life is lower than 10.
        Otherwise, Permnanent curse will be removed from the game.
    """
    # The above description will be put in reality in name and description field
    # of the ability.
    # Vars should be defined globally
    define the_answer = 42
    
    # The main block will be an event block
    on turn_start
        # Context content will be directly available inside the script
        if target.life < 10
            do player.life += 1
        else
            # This will disable the ability at the end of the turn.
            do this.counter = 0
        fi
    end

### Implementation

Ideas:
* Building of an AST
* parser should isolate each line and if first word imply the start of a block,
the next line will be stacked as children until the end of the block.
* 'on' keyword imply check if the kind of event exists
* 'if', 'elif' and 'do' wants an evaluation of the operation
* 'define' also, but to add a new var in the execution context
* 'if' and 'elif' implies running the children when the evaluation returns True
* 'else' will be run otherwise
* The context should be splitted in:
    * 'local' (vars defined inside the action script)
    * 'global' (vars which come from outside)
